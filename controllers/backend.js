module.exports = {
  hackathon: (req, res) => {
    let query = req.query
    let question = req.query.q
    let answer = ''

    if (/what is your name/.test(question)) {
      answer = 'Maroi'
    } else {
      answer = question || 'Hello Hackathon'
    }

    // Log a query from game server
    if (process.env.NODE_ENV != 'test') console.log(query) // eslint-disable-line no-console

    // Answer the question
    res.status(200).send(String(answer))
  }
}
